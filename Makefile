# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/04/17 15:56:50 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem-in

INC_DIR = includes

LIB_DIR = libft

INC_LIB_DIR = $(LIB_DIR)/$(INC_DIR)

NAME_LIB = lem_in.a\

CFLAGS = -Wall -Werror -Wextra -I$(INC_DIR) -I $(INC_LIB_DIR)

SRC = main.c ft_check_arg.c ft_check_salle_tube.c \
	  ft_lst_salle.c ft_recup_salle.c ft_verbose.c ft_display_comment.c\
	  ft_check_parsing.c ft_init.c ft_parsing.c ft_recup.c \
	  ft_recup_tube.c ft_recup_comment.c ft_display.c ft_check_min_value.c \
	  ft_create_arbre.c ft_display_tree.c ft_tree.c ft_search_path.c\
	  ft_file.c ft_lem_in.c ft_move_ants.c ft_recreate_path.c\
	  ft_create_ants.c ft_free_input.c ft_free_tree.c ft_free_ants.c\
	  ft_move_back.c ft_init2.c ft_check_path_jam.c


SRCS = $(addprefix sources/,$(SRC))

OBJ = $(SRCS:.c=.o)
CC = gcc
all : $(NAME)

$(NAME) : $(OBJ)
	(cd $(LIB_DIR) && $(MAKE))
	ar -r $(NAME_LIB) $(OBJ)
	gcc -o $(NAME) $(NAME_LIB) libft/libft.a

clean :
	(cd $(LIB_DIR) && make clean && cd ..)
	rm -rf $(OBJ)

fclean : clean
	(cd $(LIB_DIR) && make fclean && cd ..)
	rm -rf $(NAME)
	rm -rf $(NAME_LIB)

re : fclean all

.PHONY: all clean flcean re
