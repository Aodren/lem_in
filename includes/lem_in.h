/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/19 19:10:00 by abary             #+#    #+#             */
/*   Updated: 2016/04/17 15:11:07 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

/*
** ERROR DEFINE
*/

# define E_INVALID_ARG 0
# define E_MALLOC 1
# define E_PARSING 2

/*
** VERBOSE DEFINE
*/

# define V_INIT 0
# define V_DPARSING 1
# define V_FOURMIS 2
# define V_COMMANDE 3
# define V_CSALLE 4
# define V_CTUBE 5
# define V_ESALLE 6

/*
** PARSING DEFINES
*/

# define P_ERROR 0
# define P_COMMENT 1
# define P_FOURMIS 2
# define P_SALLE 3
# define P_TUBE 4
# define P_START 5
# define P_END 6
# define P_INVCOM 7

/*
** START - BEGIN
*/

# define START 1
# define END 2

typedef struct			s_salle
{
	char				*salle;
	int					x;
	int					y;
	int					begin;
	unsigned int		nbr;
	unsigned int		comment;
	int					nbrtubes;
	char				*tubecomment;
	struct s_salle		*nsalle;
	struct s_salle		*ntube;
}						t_salle;

typedef	struct			s_comment
{
	char				*comment;
	struct s_comment	*next;
}						t_comment;

typedef struct			s_tree
{
	char			*salle;
	int				begin;
	int				nbrfils;
	int				color;
	int				visit;
	int				nbrfourmis;
	struct s_tree	**next;
	struct s_tree	*pre;
}						t_tree;

typedef struct			s_path
{
	int					fourmis;
	char				*salle;
	struct s_path		*next;
}						t_path;

typedef struct			s_ants
{
	unsigned int		ant;
	int					move;
	int					arrived;
	struct s_end		*end;
	struct s_path		*bpath;
	struct s_ants		*next;

}						t_ants;

typedef struct			s_file
{
	struct s_tree		*tree;
	struct s_file		*next;
}						t_file;

typedef	struct			s_end
{
	int					size;
	struct s_tree		*end;
	struct s_end		*next;
}						t_end;

typedef struct			s_lem_in
{
	t_salle				*begin;
	t_comment			*bcomment;
	t_tree				*btree;
	unsigned int		nbrf;
	unsigned int		options;
	unsigned int		nbrtube;
	int					maxchemin;
	int					hauteur;
	int					start;
	int					end;
	int					nbrtubes;
	int					commande;
	int					endcomment;
	int					check_tube;
	int					error;
	int					checkpos;
	unsigned int		arrived;
	struct s_end		*bend;
	struct s_ants		*bant;
	char				**input;
}						t_lem_in;

/*
*******************************************************************************
**								PARSING										  *
*******************************************************************************
*/
int						ft_check_arg(char *arg);
int						ft_parsinglem_in(t_lem_in *lem_in);
int						ft_check_parsing(char *line, t_lem_in *lem_in);
int						ft_check_salle(const char *line, t_lem_in *lem_in);
int						ft_check_tube(const char *line);
int						ft_check_commande(const char *line, t_lem_in *lem_in);
int						ft_check_double_commande(int ret,
		t_lem_in *lem_in, int *commande, const char *line);
int						ft_check_min_value(t_lem_in *lem_in);
/*
*******************************************************************************
**								RECUP										  *
*******************************************************************************
*/
int						ft_recup(const char *line,
		t_lem_in *lem_in, int ret, int *commande);
int						ft_recup_salle(const char *line,
		t_lem_in *lem_in, int *commande);
int						ft_recup_tube(const char *line,
		t_lem_in *lem_in);
int						ft_recup_comment(const char *line,
		t_lem_in *lem_in);
int						ft_recup_start_end(t_lem_in *lem_in);
int						ft_lst_add_salle(t_lem_in *lem_in, t_salle *salle);
int						ft_lst_search_salle(t_lem_in *lem_in,
		t_salle *tube1, t_salle *tube2);
void					ft_begin_comment(const char *line, t_lem_in *lem_in);
/*
*******************************************************************************
**								 INIT										  *
*******************************************************************************
*/
void					ft_init_lem_in(t_lem_in *lem_in);
t_tree					*ft_init_node(t_salle *salle);
t_salle					*ft_init_salle(t_salle *salle);
t_comment				*ft_init_comment(t_comment *comment);
t_tree					*ft_init_leaf(t_salle *tube, t_salle *salle);
t_file					*ft_init_file(t_file *file);
t_end					*ft_init_end(t_end *end);
t_ants					*ft_init_t_ants(t_ants *ant);
t_path					*ft_init_path(t_path *path);
/*
*******************************************************************************
**								DISPLAY										  *
*******************************************************************************
*/
void					ft_display_file(t_lem_in *lem_in);
void					ft_display_tree(t_lem_in *lem_in);
void					ft_display_chemin(t_lem_in *lem_in);
void					ft_display_bcomment(t_lem_in *lem_in);
/*
*******************************************************************************
**								TREE										  *
*******************************************************************************
*/
void					ft_create_tree(t_lem_in *lem_in);
void					ft_fill_fils(t_salle *salle, t_tree *node);
int						ft_nbr_node(t_tree *tree);
void					ft_parcours_profondeur(t_tree *tree);
void					ft_add_node(t_salle *salle,
		t_lem_in *lem_in, t_tree *tree);
void					ft_del_useless_path(t_tree *tree);
void					ft_parcours_largeur(t_tree *tree);
void					ft_create_attente(t_tree *tree, t_file **begin);
void					ft_destruct_file(t_file *begin);
void					ft_add_file(t_tree *tree, t_file *begin);
/*
*******************************************************************************
**								PATHFINDING 								  *
*******************************************************************************
*/
int						ft_check_path_jam(t_lem_in *lem_in, t_tree *tr);
void					ft_search_path(t_tree *tree, t_lem_in *lem_in,
		int chemin);
void					ft_move_ants(t_lem_in *lem_in, int chemins);
int						ft_lem_in(t_lem_in *lem_in);
void					ft_del_bouchons(t_lem_in *lem_in);
int						ft_check_no_move(t_ants *bants);
void					ft_search_traffic_jam(t_ants *bants,
		int chemins, t_lem_in *lem_in);
void					ft_create_ants(t_lem_in *lem_in, int chemins);
void					ft_back(t_ants *last, t_ants *bants,
		t_ants *first, t_lem_in *lem_in);
/*
*******************************************************************************
**								FREE										  *
*******************************************************************************
*/
void					ft_free_path(t_ants *ant, t_path *path);
void					ft_free_input(t_lem_in *lem_in);
void					ft_free_tree(t_lem_in *lem_in);
void					ft_free_ants(t_lem_in *lem_in);
#endif
