/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/26 14:30:09 by abary             #+#    #+#             */
/*   Updated: 2016/04/17 16:47:22 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

static void	ft_display_salles(t_lem_in *lem_in)
{
	t_salle *salle;

	salle = lem_in->begin;
	while (salle)
	{
		ft_putstr(salle->salle);
		if (!salle->comment)
		{
			ft_putchar(' ');
			ft_putnbr(salle->x);
			ft_putchar(' ');
			ft_putnbr(salle->y);
		}
		ft_putchar('\n');
		salle = salle->nsalle;
	}
}

static char	*ft_display_nbr_tube(t_lem_in *lem_in,
		unsigned int nbr, char **comment)
{
	t_salle *salle;
	t_salle *tube;

	salle = lem_in->begin;
	while (salle)
	{
		tube = salle->ntube;
		while (tube)
		{
			if (tube->nbr == nbr)
			{
				if (tube->tubecomment)
					*comment = tube->tubecomment;
				return (tube->salle);
			}
			tube = tube->ntube;
		}
		salle = salle->nsalle;
	}
	return (NULL);
}

static void	ft_display_the_tube(char *name1, char *name2)
{
	ft_putstr(name1);
	ft_putchar('-');
	ft_putendl(name2);
}

static void	ft_display_tubes(t_lem_in *lem_in)
{
	unsigned int	tubes;
	char			*name1;
	char			*name2;
	char			*comment;

	tubes = 0;
	while (tubes < lem_in->nbrtube && !(comment = NULL))
	{
		if ((name1 = ft_display_nbr_tube(lem_in, tubes, &comment)))
		{
			if ((name2 = ft_display_nbr_tube(lem_in, tubes + 1, &comment)))
			{
				ft_display_the_tube(name1, name2);
				if (comment && tubes < lem_in->nbrtube - 2)
					ft_putendl(comment);
				else if (comment && lem_in->endcomment)
					ft_putendl(comment);
			}
			else
				break ;
		}
		else
			break ;
		tubes += 2;
	}
}

void		ft_display_file(t_lem_in *lem_in)
{
	ft_display_bcomment(lem_in);
	ft_putnbr(lem_in->nbrf);
	ft_putchar('\n');
	ft_display_salles(lem_in);
	ft_display_tubes(lem_in);
}
