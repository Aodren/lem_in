/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 14:02:05 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 20:42:21 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"
#include <stdlib.h>

/*
** Inutile mais je garde en ex pour parcours en hauteur
*/

void	ft_add_file(t_tree *tree, t_file *begin)
{
	t_file *file;
	t_file *tmp;

	file = NULL;
	file = ft_init_file(file);
	file->tree = tree;
	tmp = begin;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = file;
}

void	ft_create_attente(t_tree *tree, t_file **begin)
{
	t_file *file;

	file = NULL;
	*begin = ft_init_file(file);
	(*begin)->tree = tree;
}

void	ft_destruct_file(t_file *begin)
{
	if (begin->next)
		ft_destruct_file(begin->next);
	if (begin)
	{
		free(begin);
		begin = NULL;
	}
}
