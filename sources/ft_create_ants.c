/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_ants.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 22:52:51 by abary             #+#    #+#             */
/*   Updated: 2016/04/17 16:32:36 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

static void	ft_create_path(t_ants *ant, t_tree *tree)
{
	t_path *path;
	t_path *tmp;

	if (tree->pre)
		ft_create_path(ant, tree->pre);
	if (tree)
	{
		path = NULL;
		path = ft_init_path(path);
		path->salle = ft_strdup(tree->salle);
		if (!ant->bpath)
			ant->bpath = path;
		else
		{
			tmp = ant->bpath;
			while (tmp->next)
				tmp = tmp->next;
			tmp->next = path;
		}
	}
}

static void	ft_assign_path(t_ants *ant, int path, t_end *bend)
{
	t_end *tmp;

	tmp = bend;
	while (tmp->next && path)
	{
		tmp = tmp->next;
		path--;
	}
	ft_create_path(ant, tmp->end);
}

static void	ft_lst_add_ant(t_lem_in *lem_in, t_ants *ant)
{
	t_ants *tmp;

	tmp = lem_in->bant;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = ant;
}

void		ft_create_ants(t_lem_in *lem_in, int chemins)
{
	t_ants			*ants;
	int				path;
	unsigned int	ant;

	path = 0;
	ants = NULL;
	ant = 1;
	ants = ft_init_t_ants(ants);
	ants->ant = ant;
	ft_assign_path(ants, path, lem_in->bend);
	ants->bpath->fourmis = 1;
	lem_in->bant = ants;
	while (ant < lem_in->nbrf)
	{
		++ant;
		++path;
		if (path == chemins)
			path = 0;
		ants = ft_init_t_ants(ants);
		ft_assign_path(ants, path, lem_in->bend);
		ants->bpath->fourmis = 1;
		ants->ant = ant;
		ft_lst_add_ant(lem_in, ants);
	}
}
