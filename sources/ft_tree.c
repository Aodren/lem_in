/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tree.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 17:17:54 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 18:58:47 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"
#include <stdlib.h>

void			ft_fill_fils(t_salle *salle, t_tree *node)
{
	t_salle *tube;
	t_tree	*fils;
	int		i;

	i = 0;
	tube = salle->ntube;
	while (tube)
	{
		fils = ft_init_node(tube);
		node->next[i] = fils;
		tube = tube->ntube;
		++i;
	}
}

int				ft_nbr_node(t_tree *tree)
{
	int i;

	if (!tree)
		return (0);
	else
	{
		i = 0;
		while (i < tree->nbrfils)
		{
			if (tree->next[i])
				return (1 + ft_nbr_node(tree->next[i]));
			++i;
		}
	}
	return (1);
}

void			ft_parcours_profondeur(t_tree *tree)
{
	int	i;

	i = 0;
	if (tree)
	{
		if (tree->begin == START)
			ft_putendl("#start");
		else if (tree->begin == END)
			ft_putendl("#end");
		ft_putendl(tree->salle);
		while (i < tree->nbrfils)
		{
			ft_parcours_profondeur(tree->next[i]);
			++i;
		}
	}
}

void			ft_parcours_largeur(t_tree *tree)
{
	t_tree	*tmp;
	t_file	*begin;
	int		i;

	i = 0;
	begin = NULL;
	if (tree)
	{
		ft_create_attente(tree, &begin);
		while (begin)
		{
			tmp = begin->tree;
			ft_putendl(tmp->salle);
			i = 0;
			while (i < tmp->nbrfils)
			{
				if (tmp->next[i])
				{
					ft_add_file(tmp->next[i], begin);
				}
				++i;
			}
			begin = begin->next;
		}
	}
}

void			ft_add_node(t_salle *salle, t_lem_in *lem_in, t_tree *tree)
{
	int i;

	i = 0;
	if (tree)
	{
		if (ft_strequ(salle->salle, tree->salle))
		{
			if (tree->next && !tree->next[0])
			{
				ft_printf("tree %s\n", tree->salle);
				ft_printf("salle %s\n", salle->salle);
			}
			else if (!tree->next)
				ft_printf("non malloc %s\n", tree->salle);
		}
		while (i < tree->nbrfils)
		{
			ft_add_node(salle, lem_in, tree->next[i]);
			++i;
		}
	}
}
