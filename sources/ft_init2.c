/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 19:41:53 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:54:44 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

t_path	*ft_init_path(t_path *path)
{
	path = ft_memalloc(sizeof(t_path));
	path->next = NULL;
	path->salle = NULL;
	path->fourmis = 0;
	return (path);
}

t_ants	*ft_init_t_ants(t_ants *ant)
{
	ant = ft_memalloc(sizeof(t_ants));
	ant->next = NULL;
	ant->ant = 0;
	ant->end = NULL;
	ant->move = 0;
	ant->arrived = 0;
	ant->bpath = NULL;
	return (ant);
}

void	ft_init_lem_in(t_lem_in *lem_in)
{
	lem_in->nbrf = 0;
	lem_in->begin = NULL;
	lem_in->begin = 0;
	lem_in->end = 0;
	lem_in->commande = 0;
	lem_in->check_tube = 0;
	lem_in->error = 0;
	lem_in->nbrtube = 0;
	lem_in->endcomment = 0;
	lem_in->bcomment = NULL;
	lem_in->btree = NULL;
	lem_in->bend = NULL;
	lem_in->maxchemin = 0;
	lem_in->arrived = 0;
}

t_end	*ft_init_end(t_end *end)
{
	end = ft_memalloc(sizeof(t_end));
	end->next = NULL;
	end->end = NULL;
	return (end);
}

t_salle	*ft_init_salle(t_salle *salle)
{
	salle = ft_memalloc(sizeof(t_salle));
	salle->begin = 0;
	salle->comment = 0;
	salle->nbr = 0;
	salle->salle = NULL;
	salle->nsalle = NULL;
	salle->ntube = NULL;
	salle->tubecomment = NULL;
	salle->nbrtubes = 0;
	return (salle);
}
