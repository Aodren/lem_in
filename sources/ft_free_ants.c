/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_ants.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 16:42:57 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 20:40:40 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include <stdlib.h>

void		ft_free_path(t_ants *ant, t_path *path)
{
	if (path->next)
		ft_free_path(ant, path->next);
	if (path)
	{
		if (path->salle)
			free(path->salle);
		free(path);
	}
}

static void	ft_free_fourmilliere(t_ants *ants)
{
	if (ants->next)
		ft_free_fourmilliere(ants->next);
	if (ants)
	{
		if (ants->bpath)
		{
			ft_free_path(ants, ants->bpath);
			ants->bpath = NULL;
		}
		free(ants);
	}
}

void		ft_free_ants(t_lem_in *lem_in)
{
	if (lem_in->bant)
	{
		ft_free_fourmilliere(lem_in->bant);
		lem_in->bant = NULL;
	}
}
