/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_min_value.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/27 21:45:05 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 20:49:30 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		ft_check_start_end(t_lem_in *lem_in)
{
	int		start;
	int		end;
	t_salle *salle;

	start = 0;
	end = 0;
	salle = lem_in->begin;
	while (salle)
	{
		if (salle->begin == END)
			end++;
		if (salle->begin == START)
			start++;
		if (start && end)
			return (1);
		salle = salle->nsalle;
	}
	return (0);
}

int		ft_check_min_value(t_lem_in *lem_in)
{
	if (lem_in->nbrf <= 0)
		return (0);
	if (!ft_check_start_end(lem_in))
		return (0);
	return (1);
}
