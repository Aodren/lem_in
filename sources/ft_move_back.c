/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_back.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 18:48:58 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:44:22 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

int			ft_check_back(t_path *path, t_ants *bants)
{
	t_ants *ants;
	t_path *tmp;

	ants = bants;
	while (ants)
	{
		tmp = ants->bpath;
		while (tmp)
		{
			if (tmp->fourmis && !ft_strcmp(path->salle, tmp->salle))
				return (0);
			tmp = tmp->next;
		}
		ants = ants->next;
	}
	return (1);
}

static void	ft_move_back(t_ants *ant, t_path *path)
{
	ant->move = 1;
	path->next->fourmis = 0;
	path->fourmis = 1;
}

static void	ft_move_back_all(t_ants *first, t_ants *bants, t_lem_in *lem_in)
{
	t_path *path;

	if (bants->next)
		ft_move_back_all(first, bants->next, lem_in);
	if (bants)
	{
		if (bants != first && !bants->move && !bants->arrived)
		{
			path = bants->bpath;
			while (path->next)
			{
				if (path->next->fourmis)
				{
					if (ft_check_back(path, bants))
					{
						ft_printf("L%d%s-%s ",
								bants->ant, path->next->salle, path->salle);
						ft_move_back(bants, path);
						break ;
					}
				}
				path = path->next;
			}
		}
	}
}

void		ft_back(t_ants *last, t_ants *bants, t_ants *first,
		t_lem_in *lem_in)
{
	t_path *path;

	path = last->bpath;
	while (path->next)
	{
		if (path->next->fourmis)
		{
			if (ft_check_back(path, bants))
			{
				ft_printf("L%d%s-%s ",
						last->ant, path->next->salle, path->salle);
				path->fourmis = 1;
				path->next->fourmis = 0;
				last->move = 1;
				return ;
			}
		}
		path = path->next;
	}
	ft_move_back_all(first, bants, lem_in);
}
