/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/19 19:09:38 by abary             #+#    #+#             */
/*   Updated: 2016/04/03 15:23:31 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "lem_in.h"
#include <stdlib.h>

int		main(void)
{
	t_lem_in *lem_in;

	lem_in = NULL;
	if (!(lem_in = ft_memalloc(sizeof(t_lem_in))))
		return (0);
	ft_init_lem_in(lem_in);
	if (ft_parsinglem_in(lem_in))
	{
		ft_create_tree(lem_in);
		if (!ft_lem_in(lem_in))
			ft_putendl("ERROR");
	}
	else
		ft_free_input(lem_in);
	if (lem_in)
		free(lem_in);
	return (0);
}
