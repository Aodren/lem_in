/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_salle.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 15:54:06 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:51:21 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

/*
** Return 1 si l'insertion a marcher
** Return 2 si l'element existe deja
*/

int			ft_lst_add_salle(t_lem_in *lem_in, t_salle *salle)
{
	t_salle *tmp;

	tmp = (lem_in->begin);
	if (!tmp)
	{
		lem_in->begin = salle;
		return (1);
	}
	else
	{
		if (!salle->comment && ft_strequ(tmp->salle, salle->salle))
			return (0);
		while (tmp->nsalle)
		{
			if (!salle->comment && ft_strequ(tmp->salle, salle->salle))
				return (0);
			tmp = tmp->nsalle;
		}
		tmp->nsalle = salle;
	}
	return (1);
}

static void	ft_lst_add_tube(t_salle *salle, t_salle *tube)
{
	t_salle *tmp;

	tmp = salle;
	if (!tmp)
		salle->ntube = tube;
	else
	{
		while (tmp->ntube)
			tmp = tmp->ntube;
		tmp->ntube = tube;
	}
}

int			ft_lst_search_salle(t_lem_in *lem_in,
		t_salle *tube1, t_salle *tube2)
{
	int		i;
	t_salle *tmp;

	i = 0;
	tmp = (lem_in->begin);
	while (tmp)
	{
		if (ft_strequ(tmp->salle, tube1->salle))
		{
			tube2->nbr = lem_in->nbrtube + 1;
			ft_lst_add_tube(tmp, tube2);
			++i;
		}
		if (ft_strequ(tmp->salle, tube2->salle))
		{
			tube1->nbr = lem_in->nbrtube;
			ft_lst_add_tube(tmp, tube1);
			++i;
		}
		if (i == 2)
			break ;
		tmp = tmp->nsalle;
	}
	lem_in->nbrtube += 2;
	return (1);
}
