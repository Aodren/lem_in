/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_arbre.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 14:14:47 by abary             #+#    #+#             */
/*   Updated: 2016/04/03 15:11:46 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"
#include <limits.h>

static t_tree	*ft_add_fils(t_tree *tree, t_lem_in *lem_in, t_salle *salle)
{
	t_salle *tube;
	t_salle *lst;
	t_tree	*node;
	int		i;

	i = 0;
	tube = salle->ntube;
	while (tube)
	{
		lst = lem_in->begin;
		while (lst)
		{
			if (ft_strequ(tube->salle, lst->salle))
			{
				node = ft_init_node(lst);
				tree->next[i] = node;
				++i;
				break ;
			}
			lst = lst->nsalle;
		}
		tube = tube->ntube;
	}
	return (tree);
}

static void		ft_create_root(t_lem_in *lem_in)
{
	t_tree	*root;
	t_salle	*salle;

	salle = lem_in->begin;
	while (salle)
	{
		if (salle->begin == START)
		{
			root = ft_init_node(salle);
			root = ft_add_fils(root, lem_in, salle);
			lem_in->btree = root;
			break ;
		}
		salle = salle->nsalle;
	}
}

static void		ft_add_leafs(t_tree *tree,
		t_salle *tube, t_salle *salle, t_lem_in *lem_in)
{
	int		i;
	t_tree	*leaf;

	i = -1;
	if (tree && lem_in->hauteur > 0)
	{
		lem_in->hauteur--;
		if (tree->begin == END || ft_strequ(tube->salle, tree->salle))
			return ;
		if (ft_strequ(salle->salle, tree->salle))
		{
			if (!tree->next[lem_in->nbrtubes])
			{
				leaf = ft_init_leaf(tube, salle);
				tree->next[lem_in->nbrtubes] = leaf;
				lem_in->checkpos = 1;
				return ;
			}
			else
				return ;
		}
		while (++i < tree->nbrfils)
			ft_add_leafs(tree->next[i], tube, salle, lem_in);
	}
}

static void		ft_create_leafs(t_lem_in *lem_in, t_tree *tree)
{
	t_salle *salle;
	t_salle *tube;

	lem_in->checkpos = 1;
	while (lem_in->checkpos)
	{
		lem_in->checkpos = 0;
		salle = lem_in->begin;
		while (salle)
		{
			tube = salle->ntube;
			lem_in->nbrtubes = 0;
			while (tube)
			{
				ft_add_leafs(tree, tube, salle, lem_in);
				lem_in->nbrtubes++;
				tube = tube->ntube;
			}
			salle = salle->nsalle;
		}
	}
}

void			ft_create_tree(t_lem_in *lem_in)
{
	t_file *begin;

	begin = NULL;
	lem_in->hauteur = INT_MAX;
	ft_create_root(lem_in);
	ft_create_leafs(lem_in, lem_in->btree);
	ft_search_path(lem_in->btree, lem_in, 0);
}
