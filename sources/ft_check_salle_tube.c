/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_salle_tube.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 19:15:53 by abary             #+#    #+#             */
/*   Updated: 2016/04/03 15:18:39 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "lem_in.h"

int		ft_check_tube(const char *line)
{
	char *d;

	d = (char *)line;
	while (*d)
	{
		if (*d == '-')
			return (P_TUBE);
		++d;
	}
	return (P_ERROR);
}

int		ft_check_salle(const char *line, t_lem_in *lem_in)
{
	char	*d;

	d = (char *)line;
	if (*d == 'L')
	{
		lem_in->error = 1;
		return (0);
	}
	while (*d)
		++d;
	--d;
	while (ft_isdigit(*d) && (d - line) >= 0)
	{
		--d;
	}
	if (*d-- != ' ')
		return (P_ERROR);
	while (ft_isdigit(*d) && (d - line) >= 0)
	{
		--d;
	}
	if (*d != ' ')
		return (P_ERROR);
	return (P_SALLE);
}
