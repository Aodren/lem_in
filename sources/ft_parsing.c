/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 16:28:32 by abary             #+#    #+#             */
/*   Updated: 2016/04/03 15:23:53 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "lem_in.h"
#include <stdlib.h>
#include <unistd.h>

static int	ft_return_read(int retour, char *line)
{
	if (line)
		free(line);
	return (retour);
}

static int	ft_read_input(t_lem_in *lem_in)
{
	char	*line;
	int		ret;
	int		commande;

	line = NULL;
	commande = 0;
	while ((get_next_line(0, &line)) == 1 && *line)
	{
		ret = ft_check_parsing(line, lem_in);
		if (ret == P_START || ret == P_END || ret == P_FOURMIS)
		{
			if (!(ft_check_double_commande(ret, lem_in, &commande, line)))
				break ;
		}
		else if (ret == P_ERROR || ret == P_INVCOM ||
				!(ft_recup(line, lem_in, ret, &commande)))
			break ;
		free(line);
		line = NULL;
	}
	return (ft_return_read(1, line));
}

int			ft_parsinglem_in(t_lem_in *lem_in)
{
	int		i;

	i = 1;
	ft_read_input(lem_in);
	if (!ft_check_min_value(lem_in))
	{
		ft_putendl("ERROR");
		return (0);
	}
	return (1);
}
