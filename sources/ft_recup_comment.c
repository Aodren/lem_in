/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recup_comment.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/26 14:03:09 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:39:41 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"
#include <stdlib.h>

static int	ft_join_comment(const char *line, t_salle *tube)
{
	char *d;
	char n[2];

	n[0] = '\n';
	n[1] = '\0';
	d = NULL;
	if ((tube->tubecomment))
	{
		d = tube->tubecomment;
		if (!(tube->tubecomment = ft_strjoin(tube->tubecomment, n)))
			return (0);
		free(d);
		d = NULL;
		d = tube->tubecomment;
		if (!(tube->tubecomment = ft_strjoin(tube->tubecomment, line)))
			return (0);
		free(d);
	}
	else
	{
		if (!(tube->tubecomment = ft_strdup(line)))
			return (0);
	}
	return (1);
}

static int	ft_add_comment_tube(const char *line, t_lem_in *lem_in)
{
	t_salle *salle;
	t_salle	*tube;

	salle = lem_in->begin;
	while (salle)
	{
		tube = salle->ntube;
		while (tube)
		{
			if (tube->nbr == lem_in->nbrtube - 2)
			{
				if (!ft_join_comment(line, tube))
					return (0);
				return (1);
			}
			tube = tube->ntube;
		}
		salle = salle->nsalle;
	}
	return (0);
}

int			ft_recup_comment(const char *line, t_lem_in *lem_in)
{
	t_salle *new;

	new = NULL;
	if (lem_in->check_tube)
	{
		if (!ft_add_comment_tube(line, lem_in))
			return (0);
		else
			return (1);
	}
	else
	{
		if (!(new = ft_init_salle(new)))
			return (0);
		new->salle = ft_strdup(line);
		new->comment = 1;
		if (!ft_lst_add_salle(lem_in, new))
			return (0);
	}
	return (1);
}

static void	ft_lst_add_begin_comment(t_lem_in *lem_in, t_comment *new)
{
	t_comment *tmp;

	if (!lem_in->bcomment)
		lem_in->bcomment = new;
	else
	{
		tmp = lem_in->bcomment;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}

void		ft_begin_comment(const char *line, t_lem_in *lem_in)
{
	t_comment *new;

	new = NULL;
	new = ft_init_comment(new);
	new->comment = ft_strdup(line);
	ft_lst_add_begin_comment(lem_in, new);
}
