/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_path_jam.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/17 14:55:05 by abary             #+#    #+#             */
/*   Updated: 2016/04/17 16:48:49 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

int		ft_check_path_jam(t_lem_in *lem_in, t_tree *tr)
{
	t_end	*tmp;
	t_tree	*tree;
	t_tree	*pre;

	if (!lem_in->bend)
		return (1);
	tmp = lem_in->bend;
	while (tmp)
	{
		tree = tmp->end;
		while (tree)
		{
			pre = tr;
			while (pre)
			{
				if (!ft_strcmp(pre->salle, tree->salle) && pre->begin == 0)
					return (0);
				pre = pre->pre;
			}
			tree = tree->pre;
		}
		tmp = tmp->next;
	}
	return (1);
}
