/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 17:09:28 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:43:32 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

t_comment	*ft_init_comment(t_comment *comment)
{
	comment = ft_memalloc(sizeof(t_comment));
	comment->comment = NULL;
	comment->next = NULL;
	return (comment);
}

t_tree		*ft_init_leaf(t_salle *tube, t_salle *salle)
{
	t_tree	*leaf;
	int		i;

	i = 0;
	leaf = ft_memalloc(sizeof(t_tree));
	if (salle->nbrtubes)
		leaf->next = ft_memalloc(sizeof(t_tree *) * salle->nbrtubes);
	else
		leaf->next = NULL;
	leaf->salle = ft_strdup(tube->salle);
	leaf->nbrfils = salle->nbrtubes;
	leaf->begin = tube->begin;
	leaf->color = 0;
	leaf->nbrfourmis = 0;
	leaf->pre = NULL;
	leaf->visit = 0;
	while (i < leaf->nbrfils)
	{
		leaf->next[i] = NULL;
		++i;
	}
	return (leaf);
}

t_tree		*ft_init_node(t_salle *salle)
{
	t_tree	*node;
	int		i;

	node = ft_memalloc(sizeof(t_tree));
	if (salle->nbrtubes)
		node->next = ft_memalloc(sizeof(t_tree *) * salle->nbrtubes);
	else
		node->next = NULL;
	node->salle = ft_strdup(salle->salle);
	node->nbrfils = salle->nbrtubes;
	node->begin = salle->begin;
	node->color = 0;
	node->nbrfourmis = 0;
	node->visit = 0;
	node->pre = NULL;
	i = 0;
	while (i < node->nbrfils)
	{
		node->next[i] = NULL;
		++i;
	}
	return (node);
}

t_file		*ft_init_file(t_file *file)
{
	file = ft_memalloc(sizeof(t_file));
	file->next = NULL;
	file->tree = NULL;
	return (file);
}
