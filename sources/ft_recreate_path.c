/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recreate_path.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 22:44:39 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 21:01:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

int			ft_check_no_move(t_ants *bants)
{
	t_ants *tmp;

	tmp = bants;
	while (tmp)
	{
		if (tmp->move == 1)
			return (0);
		tmp = tmp->next;
	}
	return (1);
}

static void	ft_recreate_path(t_ants *ant, t_path *new, t_path *path)
{
	t_path	*tmp;
	t_path	*tmpnew;
	t_path	*parcours;
	char	*fr;

	fr = ft_strdup(path->salle);
	ft_free_path(ant, ant->bpath);
	tmp = NULL;
	ant->bpath = ft_init_path(tmp);
	ant->bpath->salle = ft_strdup(new->salle);
	tmpnew = new;
	while (tmpnew)
	{
		tmp = ft_init_path(tmp);
		tmp->salle = ft_strdup(tmpnew->salle);
		if (!ft_strcmp(fr, tmp->salle))
			tmp->fourmis = 1;
		parcours = ant->bpath;
		while (parcours->next)
			parcours = parcours->next;
		parcours->next = tmp;
		tmpnew = tmpnew->next;
	}
}

int			ft_change_path(t_ants *ant, t_ants *bants, t_path *path,
		int chemins)
{
	t_path *tmppath;
	t_ants *tmpant;

	tmpant = bants;
	while (tmpant && --chemins > -1)
	{
		tmppath = tmpant->bpath;
		while (tmppath)
		{
			if (!ft_strcmp(path->salle, tmppath->salle))
			{
				if (path->next && tmppath->next)
				{
					if (ft_strcmp(path->next->salle, tmppath->next->salle))
					{
						ft_recreate_path(ant, tmpant->bpath, path);
						return (1);
					}
				}
			}
			tmppath = tmppath->next;
		}
		tmpant = tmpant->next;
	}
	return (0);
}

t_ants		*ft_parcours_traffic(t_ants *tmp, t_ants **first,
		int chemins, t_ants *bants)
{
	t_ants *last;
	t_path *path;

	if (!tmp->arrived)
	{
		if (!first)
			*first = tmp;
		path = tmp->bpath;
		while (path)
		{
			if (path->fourmis)
			{
				if (ft_change_path(tmp, bants, path, chemins))
					return (NULL);
				else
					return (tmp);
				last = tmp;
			}
			path = path->next;
		}
	}
	return (NULL);
}

void		ft_search_traffic_jam(t_ants *bants, int chemins, t_lem_in *lem_in)
{
	t_ants *tmp;
	t_ants *last;
	t_ants *first;

	tmp = bants;
	first = NULL;
	while (tmp)
	{
		if (!(last = ft_parcours_traffic(tmp, &first, chemins, bants)))
			return ;
		tmp = tmp->next;
	}
	ft_back(last, bants, first, lem_in);
	ft_putendl("");
}
