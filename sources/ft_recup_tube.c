/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recup_tube.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 17:30:42 by abary             #+#    #+#             */
/*   Updated: 2016/04/03 15:25:28 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

static int	ft_verif_str(const char *str, t_salle *salle)
{
	size_t		i;
	size_t		size;

	size = ft_strlen(salle->salle);
	i = 0;
	while (salle->salle[i] && str[i] && salle->salle[i] == str[i])
		++i;
	if ((!str[i] && !salle->salle[i]) || (!salle->salle[i] && str[i] == '-'))
		return (1);
	else
		return (0);
}

static int	ft_verif_size(t_salle *tube1, t_salle *tmp, const char *line)
{
	if (ft_strlen(tube1->salle) + ft_strlen(tmp->salle) == ft_strlen(line) - 1)
		return (1);
	else
		return (0);
}

static int	ft_add_tube(t_salle **tmp, t_salle *tube, t_lem_in *lem_in)
{
	(*tmp)->nbrtubes++;
	tube->salle = ft_strdup((*tmp)->salle);
	tube->begin = (*tmp)->begin;
	*tmp = lem_in->begin;
	return (1);
}

static int	ft_recup_tubes(const char *line, t_salle *tube1, t_lem_in *lem_in,
		t_salle *tube2)
{
	t_salle *tmp;
	char	*str;
	size_t	size;

	tmp = (lem_in->begin);
	str = (char *)line;
	while (tmp && (size = ft_strlen(tmp->salle)))
	{
		if (!tmp->comment && ft_strnequ(tmp->salle, str, size))
		{
			if (ft_verif_str(str, tmp))
			{
				str = str + size + 1;
				if (tube1->salle == NULL && ft_add_tube(&tmp, tube1, lem_in))
					continue ;
				else if (ft_verif_size(tube1, tmp, line) &&
						ft_add_tube(&tmp, tube2, lem_in))
					return (1);
				else
					str = (char*)line + ft_strlen(tube1->salle);
			}
		}
		tmp = tmp->nsalle;
	}
	return (0);
}

int			ft_recup_tube(const char *line, t_lem_in *lem_in)
{
	t_salle *tube1;
	t_salle *tube2;

	tube1 = NULL;
	tube2 = NULL;
	if (!(tube1 = ft_init_salle(tube1)))
		return (0);
	if (!(tube2 = ft_init_salle(tube2)))
		return (0);
	if (!(ft_recup_tubes(line, tube1, lem_in, tube2)))
		return (0);
	if (!ft_lst_search_salle(lem_in, tube1, tube2))
		return (0);
	return (1);
}
