/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_input.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 15:17:28 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:56:20 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include <stdlib.h>

static void	ft_free_bcomment(t_comment *bcomment)
{
	if (bcomment->next)
		ft_free_bcomment(bcomment->next);
	if (bcomment)
	{
		if (bcomment->comment)
		{
			free(bcomment->comment);
			bcomment->comment = NULL;
			free(bcomment);
			bcomment = NULL;
		}
	}
}

static void	ft_free_tubes(t_salle *salle)
{
	if (salle->ntube)
		ft_free_tubes(salle->ntube);
	if (salle)
	{
		if (salle->salle)
		{
			free(salle->salle);
			salle->salle = NULL;
		}
		if (salle->tubecomment)
		{
			free(salle->tubecomment);
			salle->tubecomment = NULL;
		}
		free(salle);
		salle = NULL;
	}
}

static void	ft_free_salle(t_salle *salle)
{
	if (salle->nsalle)
		ft_free_salle(salle->nsalle);
	if (salle)
	{
		if (salle->ntube)
		{
			ft_free_tubes(salle->ntube);
			salle->ntube = NULL;
		}
		if (salle->salle)
		{
			free(salle->salle);
			salle->salle = NULL;
		}
		if (salle->tubecomment)
		{
			free(salle->tubecomment);
			salle->tubecomment = NULL;
		}
		free(salle);
		salle = NULL;
	}
}

void		ft_free_input(t_lem_in *lem_in)
{
	if (lem_in->bcomment)
	{
		ft_free_bcomment(lem_in->bcomment);
		lem_in->bcomment = NULL;
	}
	if (lem_in->begin)
	{
		ft_free_salle(lem_in->begin);
		lem_in->begin = NULL;
	}
}
