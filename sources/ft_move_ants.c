/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_ants.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 16:45:23 by abary             #+#    #+#             */
/*   Updated: 2016/04/03 20:57:04 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

static void		ft_move_zero(t_ants *bant)
{
	t_ants *ants;

	ants = bant;
	while (ants)
	{
		ants->move = 0;
		ants = ants->next;
	}
}

int				ft_check_salle_occuped(t_path *next,
		t_ants *all_ants, t_lem_in *lem_in, t_ants *ant)
{
	t_ants	*tmp;
	t_path	*tmpath;

	if (!next->next)
	{
		ant->arrived = 1;
		lem_in->arrived++;
		return (1);
	}
	else
	{
		tmp = all_ants;
		while (tmp)
		{
			tmpath = tmp->bpath;
			while (tmpath)
			{
				if (tmpath->fourmis && !ft_strcmp(tmpath->salle, next->salle))
					return (0);
				tmpath = tmpath->next;
			}
			tmp = tmp->next;
		}
	}
	return (1);
}

int				ft_move_the_ant(t_ants *ant, t_ants *all_ants,
		t_lem_in *lem_in)
{
	t_path *path;

	path = ant->bpath;
	while (path->next)
	{
		if (path->fourmis)
		{
			if (ft_check_salle_occuped(path->next, all_ants, lem_in, ant))
			{
				path->fourmis = 0;
				path->next->fourmis = 1;
				ant->move = 1;
				ft_printf("L%d-%s ",
						ant->ant, path->next->salle);
				return (1);
			}
		}
		path = path->next;
	}
	return (0);
}

void			ft_move(t_lem_in *lem_in, t_ants *bant)
{
	t_ants	*ants;

	ants = bant;
	while (ants)
	{
		if (!ants->arrived)
		{
			if (ants->move == 0 && ft_move_the_ant(ants, bant, lem_in))
				ft_move(lem_in, bant);
		}
		ants = ants->next;
	}
}

void			ft_move_ants(t_lem_in *lem_in, int chemins)
{
	int end;

	end = 0;
	ft_create_ants(lem_in, chemins);
	ft_free_tree(lem_in);
	while (lem_in->arrived < lem_in->nbrf)
	{
		ft_move_zero(lem_in->bant);
		ft_move(lem_in, lem_in->bant);
		if (ft_check_no_move(lem_in->bant))
			ft_search_traffic_jam(lem_in->bant, chemins, lem_in);
		else
			ft_putchar('\n');
	}
}
