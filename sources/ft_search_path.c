/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_search_path.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 18:46:53 by abary             #+#    #+#             */
/*   Updated: 2016/04/17 16:17:12 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include <unistd.h>
#include "libft.h"

static void	ft_lst_add_no_tmp(t_end *end, t_lem_in *lem_in, t_end *tmp)
{
	end->next = tmp;
	lem_in->bend = end;
}

static void	ft_lst_add_end(t_end *tmp2, t_end *end, t_end *tmp)
{
	tmp2->next = end;
	end->next = tmp;
}

static void	ft_lst_end(t_lem_in *lem_in, t_tree *tree, int size)
{
	t_end	*end;
	t_end	*tmp;
	t_end	*tmp2;

	end = NULL;
	end = ft_init_end(end);
	end->end = tree;
	end->size = size;
	if (!lem_in->bend)
		lem_in->bend = end;
	else
	{
		tmp = lem_in->bend;
		tmp2 = NULL;
		while (tmp && tmp->size < size)
		{
			tmp2 = tmp;
			tmp = tmp->next;
		}
		if (!tmp2)
			ft_lst_add_no_tmp(end, lem_in, tmp);
		else
			ft_lst_add_end(tmp2, end, tmp);
	}
}

void		ft_search_path(t_tree *tree, t_lem_in *lem_in, int chemin)
{
	int i;

	i = 0;
	if (tree && tree->visit == 0)
	{
		++chemin;
		tree->visit = 1;
		if (tree->begin == END)
		{
			if (ft_check_path_jam(lem_in, tree))
			{
				++lem_in->maxchemin;
				ft_lst_end(lem_in, tree, chemin);
			}
		}
		while (i < tree->nbrfils)
		{
			if (tree->next[i])
				tree->next[i]->pre = tree;
			ft_search_path(tree->next[i], lem_in, chemin);
			++i;
		}
	}
}
