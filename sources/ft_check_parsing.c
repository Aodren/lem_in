/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_parsing.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 18:14:44 by abary             #+#    #+#             */
/*   Updated: 2016/04/17 16:48:20 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"
#include <stdlib.h>

static int	ft_recup_fourmis(const char *line, t_lem_in *lem_in)
{
	long fourmis;
	char *d;

	if (ft_strlen(line) > 10)
		return (P_ERROR);
	d = (char *)line;
	if (*d == '+')
		++d;
	while (*d)
	{
		if (!(ft_isdigit(*d)))
			return (P_ERROR);
		++d;
	}
	fourmis = ft_atol(line);
	if (fourmis > 2147483647)
		return (P_ERROR);
	else
		lem_in->nbrf = fourmis;
	return (P_FOURMIS);
}

static int	ft_check_comment(const char *line)
{
	if (*line == '#')
		return (1);
	return (0);
}

int			ft_check_double_commande(int ret, t_lem_in *lem_in,
		int *commande, const char *line)
{
	if (ret == P_COMMENT || ret == P_FOURMIS)
		return (1);
	if (ret == P_START)
	{
		*commande = 1;
		if (lem_in->start == 0)
		{
			lem_in->start = 1;
			ft_recup_comment(line, lem_in);
			return (1);
		}
		else
			return (0);
	}
	else if (lem_in->end == 0)
	{
		*commande = 2;
		lem_in->end = 1;
		ft_recup_comment(line, lem_in);
		return (1);
	}
	return (0);
}

int			ft_check_commande(const char *line, t_lem_in *lem_in)
{
	if (*(line + 1) != '#' || (*(line + 1) == '#' && *(line + 2) == '#'))
		return (0);
	if (ft_strequ(line, "##start"))
	{
		if (lem_in->start == 1)
			return (-1);
		else
			return (P_START);
	}
	else if (ft_strequ(line, "##end"))
	{
		if (lem_in->end == 1)
			return (-1);
		else
			return (P_END);
	}
	else
		return (1);
}

int			ft_check_parsing(char *line, t_lem_in *lem_in)
{
	int ret;

	if (ft_check_comment(line))
	{
		if ((ret = ft_check_commande(line, lem_in)) != 0)
		{
			if (lem_in->nbrf != 0)
				return (ret);
			else
				return (0);
		}
		return (P_COMMENT);
	}
	if (lem_in->nbrf == 0)
		return (ft_recup_fourmis(line, lem_in));
	if (lem_in->check_tube == 0)
	{
		if (ft_check_salle(line, lem_in))
			return (P_SALLE);
		if (lem_in->error)
			return (0);
	}
	lem_in->check_tube = 1;
	return (ft_check_tube(line));
}
