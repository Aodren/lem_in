/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recup_salle.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 15:41:58 by abary             #+#    #+#             */
/*   Updated: 2016/04/03 13:40:14 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"
#include "limits.h"
#include <stdlib.h>

/*
**	return 1 si l'insertion a reussit
**  return 0 si l'element existe deja dans la liste
*/

static int		ft_recup_int(const char *line, t_salle *salle, int x)
{
	long nbr;

	nbr = ft_atol(line);
	if (nbr >= INT_MAX || nbr <= INT_MIN)
		return (0);
	if (x)
		salle->x = (int)nbr;
	else
		salle->y = (int)nbr;
	return (1);
}

static t_salle	*ft_name_salle(const char *line, t_salle *new)
{
	char	*name;
	size_t	size;

	size = ft_strlen(line);
	name = (char *)line + size - 1;
	while (ft_isdigit(*name))
	{
		--name;
		--size;
	}
	--name;
	--size;
	while (ft_isdigit(*name))
	{
		--size;
		--name;
	}
	--size;
	new->salle = ft_strndup(line, size);
	return (new);
}

static int		ft_recup_coord(const char *line, t_salle *salle)
{
	char	*coord;
	size_t	size;

	size = ft_strlen(line);
	coord = (char *)line + size - 1;
	while (coord - size > 0 && ft_isdigit(*coord))
		--coord;
	if (coord - line > -1 && *coord == ' ' && ft_isdigit(*(coord - 1)))
	{
		if (!ft_recup_int(coord--, salle, 0))
			return (0);
	}
	else
		return (0);
	while (coord - line > 0 && ft_isdigit(*coord))
		--coord;
	if (coord - line > 0 && *coord == ' ')
	{
		if (!ft_recup_int(coord, salle, 1))
			return (0);
	}
	else
		return (0);
	return (1);
}

int				ft_recup_salle(const char *line, t_lem_in *lem_in,
		int *commande)
{
	t_salle *new;

	new = NULL;
	if (!(new = ft_init_salle(new)))
		return (0);
	new = ft_name_salle(line, new);
	new->begin = *commande;
	*commande = 0;
	if (!(ft_recup_coord(line, new)))
		return (0);
	return (ft_lst_add_salle(lem_in, new));
}
