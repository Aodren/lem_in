/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_verbose.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 18:06:10 by abary             #+#    #+#             */
/*   Updated: 2016/03/23 21:52:17 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

void	ft_verbose(int verbose)
{
	if (verbose == V_INIT)
		ft_putendl("struct init");
	else if (verbose == V_DPARSING)
		ft_putendl("Beginning of the parsing");
	else if (verbose == V_FOURMIS)
		ft_putendl("Recover nbr of ants");
	else if (verbose == V_COMMANDE)
		ft_putendl("Check Command");
	else if (verbose == V_CSALLE)
		ft_putendl("Check Room");
	else if (verbose == V_CTUBE)
		ft_putendl("CHEck Tube");
	else if (verbose == V_ESALLE)
		ft_putendl("END check room");
}
