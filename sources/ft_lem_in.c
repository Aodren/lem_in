/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lem_in.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 19:57:16 by abary             #+#    #+#             */
/*   Updated: 2016/04/17 16:35:59 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

int		ft_lem_in(t_lem_in *lem_in)
{
	int nbrchemins;

	if (!lem_in->bend)
	{
		ft_free_tree(lem_in);
		return (0);
	}
	ft_display_file(lem_in);
	ft_free_input(lem_in);
	nbrchemins = lem_in->maxchemin % lem_in->nbrf;
	if (nbrchemins <= 0)
		nbrchemins = lem_in->maxchemin;
	ft_putendl("");
	ft_move_ants(lem_in, nbrchemins);
	ft_free_ants(lem_in);
	return (1);
}
