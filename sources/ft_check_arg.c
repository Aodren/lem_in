/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_arg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 16:54:23 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 20:59:41 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

int		ft_check_arg(char *arg)
{
	if (ft_strlen(arg) == 2 && *arg == '-' && *(arg + 1) == 'v')
		return (1);
	return (0);
}
