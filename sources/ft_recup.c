/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recup.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/23 22:09:16 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:48:28 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

int		ft_recup(const char *line, t_lem_in *lem_in, int ret, int *commande)
{
	if (ret == P_COMMENT)
	{
		lem_in->endcomment = 1;
		if (lem_in->nbrf == 0)
		{
			ft_begin_comment(line, lem_in);
			return (1);
		}
		return (ft_recup_comment(line, lem_in));
	}
	if (ret == P_SALLE)
		return (ft_recup_salle(line, lem_in, commande));
	if (ret == P_TUBE)
	{
		lem_in->endcomment = 0;
		return (ft_recup_tube(line, lem_in));
	}
	return (0);
}
