/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_tree.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/28 18:07:59 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 20:44:05 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

void	ft_display_tree(t_lem_in *lem_in)
{
	t_tree	*tree;
	int		i;

	i = 0;
	tree = lem_in->btree;
	ft_putendl(tree->salle);
	while (i < tree->nbrfils)
	{
		ft_putendl(tree->next[i]->salle);
		++i;
	}
}

void	ft_display_chemin(t_lem_in *lem_in)
{
	t_end	*end;
	t_tree	*tree;

	end = lem_in->bend;
	while (end)
	{
		tree = end->end;
		ft_printf("taille du chemin : %d\n", end->size);
		while (tree)
		{
			ft_putendl(tree->salle);
			tree = tree->pre;
		}
		ft_putendl("");
		end = end->next;
	}
}
