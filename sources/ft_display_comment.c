/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_comment.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 20:34:28 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 20:38:15 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include "libft.h"

void	ft_display_bcomment(t_lem_in *lem_in)
{
	t_comment *comment;

	comment = lem_in->bcomment;
	while (comment)
	{
		ft_putendl(comment->comment);
		comment = comment->next;
	}
}
