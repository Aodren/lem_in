/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_tree.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 16:08:51 by abary             #+#    #+#             */
/*   Updated: 2016/04/02 19:54:24 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include <stdlib.h>

static void	ft_free_end(t_end *end)
{
	if (end->next)
		ft_free_end(end->next);
	if (end)
	{
		free(end);
		end = NULL;
	}
}

static void	ft_free_all_tree(t_tree *tree)
{
	int i;

	i = 0;
	if (tree)
	{
		while (i < tree->nbrfils)
		{
			ft_free_all_tree(tree->next[i]);
			++i;
		}
	}
	if (tree)
	{
		if (tree->salle)
			free(tree->salle);
		if (tree->next)
		{
			free(tree->next);
			tree->next = NULL;
		}
		free(tree);
		tree = NULL;
	}
}

void		ft_free_tree(t_lem_in *lem_in)
{
	if (lem_in->btree)
	{
		ft_free_all_tree(lem_in->btree);
		lem_in->btree = NULL;
	}
	if (lem_in->bend)
	{
		ft_free_end(lem_in->bend);
		lem_in->bend = NULL;
	}
}
